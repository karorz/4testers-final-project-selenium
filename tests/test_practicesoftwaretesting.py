import string
import random

import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager

from pages.login_page import LoginPage

administrator_email = 'customer@practicesoftwaretesting.com'
administrator_password = 'welcome01'


@pytest.fixture
def browser():
    # Część która wykona się przed każdym testem
    service = Service(ChromeDriverManager().install())
    browser = Chrome(service=service)
    browser.set_window_size(1920, 1080)

    login_page = LoginPage(browser)
    login_page.visit()

    yield browser
    browser.quit()


def test_practicesoftwaretesting_login(browser):
    login_page = LoginPage(browser)
    login_page.login(administrator_email, administrator_password)
    assert 'account' in browser.current_url


def test_practicesoftwaretesting_login_missing_email(browser):
    login_page = LoginPage(browser)
    login_page.login('', administrator_password)
    assert browser.find_element(By.CSS_SELECTOR, '.alert').is_displayed()


def test_practicesoftwaretesting_login_missing_password(browser):
    login_page = LoginPage(browser)
    login_page.login(administrator_email, '')
    assert browser.find_element(By.CSS_SELECTOR, '.alert').is_displayed()


def test_practicesoftwaretesting_forget_password(browser):
    login_page = LoginPage(browser)
    login_page.forgot_password(administrator_email)
    assert browser.find_element(By.CSS_SELECTOR, '.alert').is_displayed()
