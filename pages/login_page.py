import time

from selenium.webdriver.common.by import By


class LoginPage:

    # Inicjalizacja klasy - przekazanie drivera/przeglądarki
    def __init__(self, browser):
        self.browser = browser

    # otwarcie strony
    def visit(self):

        self.browser.get('https://practicesoftwaretesting.com/#/')

    def login(self, email, password):
        self.browser.find_element(By.CSS_SELECTOR, 'a[href="#/auth/login"]').click()
        time.sleep(1)
        self.browser.find_element(By.CSS_SELECTOR, '#email').send_keys(email)
        time.sleep(1)
        self.browser.find_element(By.CSS_SELECTOR, '#password').send_keys(password)
        time.sleep(1)
        self.browser.find_element(By.CSS_SELECTOR, '.btnSubmit').click()
        time.sleep(1)

    def forgot_password(self, email):
        self.browser.find_element(By.CSS_SELECTOR, 'a[href="#/auth/login"]').click()
        time.sleep(1)
        self.browser.find_element(By.CSS_SELECTOR, 'a[href="#/auth/forgot-password"]').click()
        time.sleep(1)
        self.browser.find_element(By.CSS_SELECTOR, '#email').send_keys(email)
        time.sleep(1)
        self.browser.find_element(By.CSS_SELECTOR, '.btnSubmit').click()
        time.sleep(1)